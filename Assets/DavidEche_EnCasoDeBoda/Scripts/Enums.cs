﻿public enum Cube_Action
{
    No_Action = -1,
    Move_Forward = 0,
    Turn_Left = 1,
    Turn_Right = 2,
    Turn_Down = 3
}
